package lib

import "log"

func NoErr(err error, msg string) bool {
	if err == nil {
		return true
	}
	log.Print("(", msg, ") ", err.Error())
	return false
}
