package lib

import "encoding/json"

type Config struct {
	Index []byte
	Salt  []byte
	Short string
	Title string
}

func ConfigNew() *Config {
	var c Config
	err := json.Unmarshal(Fr("html/index.json"), &c)
	NoErr(err, "")
	return &c
}
