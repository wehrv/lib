package lib

import "math/rand"

func Shuffle(a []string) []string {
	for _ = range a {
		b := rand.Intn(len(a))
		c := b
		for {
			c = rand.Intn(len(a))
			if b != c {
				break
			}
		}
		a[b], a[c] = a[c], a[b]
	}
	return a
}
