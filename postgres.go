package lib

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"sort"
	"strings"
	"time"

	gopg "gopkg.in/pg.v5"
	//gopg "github.com/go-pg/pg"
)

type Pg struct {
	Conn   pgConn
	Tables []pgTables
}

type pgConn struct {
	Host string
	Port string
	Data string
	User string
	Pass string
}

type pgTables struct {
	Table    string
	Primary  []string
	Columns  []string
	Checks   []string
	Uniques  []string
	Indexes  []string
	Defaults []string
	NotNulls []string
	Nulls    []string
	Enums    []pgEnum
}

type pgEnum struct {
	Column string
	Values []string
}

var DB *gopg.DB

func Postgres(f string) {
	var err error
	var pg Pg
	data, err := ioutil.ReadFile(f)
	PgAbt(err, []string{}, "lib.Postgres:ioutil.ReadFile")
	err = json.Unmarshal(data, &pg)
	PgAbt(err, []string{}, "lib.Postgres:json.Unmarshal")
	if pg.Conn.Host == "" {
		pg.Conn.Host = "172.17.0.1"
	}
	if pg.Conn.Port == "" {
		pg.Conn.Port = "5432"
	}
	if pg.Conn.User == "" {
		pg.Conn.User = pg.Conn.Data
	}
	DB = gopg.Connect(&gopg.Options{
		User:                  pg.Conn.User,
		Addr:                  pg.Conn.Host + ":" + pg.Conn.Port,
		Password:              pg.Conn.Pass,
		Database:              pg.Conn.Data,
		MaxRetries:            10,
		RetryStatementTimeout: true,
		PoolSize:              10,
		PoolTimeout:           time.Duration(100) * time.Second,
		IdleTimeout:           time.Duration(100) * time.Second})
	for table := range pg.Tables {
		_, err := DB.Exec("CREATE TABLE IF NOT EXISTS " + pg.Tables[table].Table + "()")
		PgAbt(err, []string{}, "lib.Postgres:CreateTable")
		sort.StringSlice(pg.Tables[table].Columns).Sort()
		for item := range pg.Tables[table].Columns {
			postgresColumn(DB, pg.Tables[table].Table, pg.Tables[table].Columns[item])
		}
		for item := range pg.Tables[table].Defaults {
			postgresDefault(DB, pg.Tables[table].Table, pg.Tables[table].Defaults[item])
		}
		//		for item := range pg.Tables[table].Checks {
		//			postgresCheck(DB, pg.Tables[table].Table, pg.Tables[table].Checks[item])
		//		}
		//		for item := range pg.Tables[table].Uniques {
		//			postgresUnique(DB, pg.Tables[table].Table, pg.Tables[table].Uniques[item])
		//		}
		for item := range pg.Tables[table].Enums {
			postgresEnum(DB, pg.Tables[table].Table, pg.Tables[table].Enums[item].Column, pg.Tables[table].Enums[item].Values)
		}
		for item := range pg.Tables[table].Indexes {
			postgresIndex(DB, pg.Tables[table].Table, pg.Tables[table].Indexes[item])
		}
		if len(pg.Tables[table].Primary) > 0 {
			postgresPrimary(DB, pg.Tables[table].Table, pg.Tables[table].Primary)
		}
		for item := range pg.Tables[table].NotNulls {
			postgresNotNull(DB, pg.Tables[table].Table, pg.Tables[table].NotNulls[item])
		}
	}
}

func postgresEnum(DB *gopg.DB, table string, column string, values []string) {
	// TODO allow dropping of a TYPE element: http://stackoverflow.com/questions/25811017/how-to-delete-an-enum-type-in-postgres
	sort.StringSlice(values).Sort()
	_, err := DB.Exec("CREATE TYPE " + table + "_" + column + " AS ENUM ('" + strings.Join(values, "','") + "')")
	PgAbt(err, []string{"42710"}, "lib.Postgres:Enum")
	postgresColumn(DB, table, column+" "+table+"_"+column)
	postgresIndex(DB, table, column)
}

func postgresColumn(DB *gopg.DB, table string, column string) {
	_, err := DB.Exec("ALTER TABLE " + table + " ADD " + column)
	PgAbt(err, []string{"42701"}, "lib.Postgres:Columns")
}

func postgresNotNull(DB *gopg.DB, table string, column string) {
	_, err := DB.Exec("ALTER TABLE " + table + " ALTER COLUMN " + column + " SET NOT NULL")
	PgAbt(err, []string{}, "lib.PostgresNotNull0")
}

func postgresNull(DB *gopg.DB, table string, column string) {
	_, err := DB.Exec("ALTER TABLE " + table + " ALTER COLUMN " + column + " DROP NOT NULL")
	PgAbt(err, []string{"42P16"}, "lib.PostgresNull0")
}

func postgresPrimary(DB *gopg.DB, table string, primary []string) {
	sort.StringSlice(primary).Sort()
	var connames []string
	cname := "primary_" + table + "_" + strings.Join(primary, "_")
	_, err := DB.Query(&connames, "SELECT conname FROM pg_constraint where conname like 'primary_"+table+"_%'")
	PgAbt(err, []string{}, "lib.Postgres:Primary0")
	var match bool
	for conname := range connames {
		if connames[conname] == cname {
			match = true
		} else {
			_, err = DB.Exec("ALTER TABLE " + table + " DROP CONSTRAINT" + cname)
			PgAbt(err, []string{}, "lib.Postgres:Primary1")
		}
	}
	if !match {
		_, err = DB.Exec("ALTER TABLE " + table + " ADD CONSTRAINT " + cname + " PRIMARY KEY (" + strings.Join(primary, ",") + ")")
		PgAbt(err, []string{}, "lib.Postgres:Primary2")
	}
}

func postgresIndex(DB *gopg.DB, table string, index string) {
	_, err := DB.Exec("CREATE INDEX " + "index_" + table + "_" + index + " ON " + table + " (" + index + ")")
	PgAbt(err, []string{"42P07"}, "lib.Postgres:Index")
}

//func postgresUnique(DB *gopg.DB, table string, unique string) {
//	cName := "unique_" + table + "_" + unique
//	_, err := DB.Exec("ALTER TABLE " + table + " ADD CONSTRAINT " + cName + " UNIQUE (" + unique + ")")
//	PgAbt(err, []string{}, "lib.Postgres:Uniques")
//}

//func postgresCheck(DB *gopg.DB, table string, check string) {
//	cName := "check_" + table + "_" + check
//	_, err := DB.Exec("ALTER TABLE " + table + " ADD check " + cName + " CHECK (" + check + ")")
//	PgAbt(err, []string{}, "lib.Postgres:Checks")
//}

func postgresDefault(DB *gopg.DB, table string, def string) {
	_, err := DB.Exec("ALTER TABLE " + table + " ALTER COLUMN " + strings.Split(def, " ")[0] + " SET DEFAULT " + strings.Join(strings.Split(def, " ")[1:], " "))
	PgAbt(err, []string{}, "lib.Postgres:Defaults")
}

func PgErr(err error, okErrors []string, msg string) bool {
	if err == nil || StringSliceHasPrefix(okErrors, err.Error()[7:]) {
		return false
	}
	NoErr(err, msg)
	return true
}

func PgAbt(err error, okErrors []string, msg string) {
	if PgErr(err, okErrors, msg) {
		os.Exit(1)
	}
}
