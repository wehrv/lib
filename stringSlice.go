package lib

import "strings"

func StringSliceHasPrefix(b []string, a string) bool {
	for c := range b {
		if strings.HasPrefix(a, b[c]) {
			return true
		}
	}
	return false
}

func StringSliceHasSuffix(b []string, a string) bool {
	for c := range b {
		if strings.HasSuffix(a, b[c]) {
			return true
		}
	}
	return false
}

func StringSliceHasString(b []string, a string) bool {
	for c := range b {
		if a == b[c] {
			return true
		}
	}
	return false
}

func StringSliceContains(b []string, a string) bool {
	for c := range b {
		if strings.Contains(a, b[c]) {
			return true
		}
	}
	return false
}

func StringHasSuffixes(a string, b []string) bool {
	var match bool
	for c := range b {
		if strings.HasSuffix(a, b[c]) {
			match = true
			break
		}
	}
	return match
}
