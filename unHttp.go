package lib

import "strings"

func UnHttp(a string) string {
	if strings.HasPrefix(a, "http://") {
		return a[7:]
	}
	if strings.HasPrefix(a, "https://") {
		return a[8:]
	}
	return a
}
