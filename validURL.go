package lib

import "net/url"

func ValidUrl(link string) bool {
	u, err := url.Parse(link)
	if err == nil && (u.Scheme == "http" || u.Scheme == "https") && u.Host != "" {
		return true
	}
	return false
}
