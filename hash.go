package lib

import (
	"encoding/binary"
	"encoding/hex"

	"github.com/dchest/blake2b"
)

func HashRaw(a []byte) []byte {
	b, _ := blake2b.New(nil)
	b.Write(a)
	return b.Sum(nil)
}

func HashBin(a []byte) []byte {
	return HashRaw(a)
}

func HashHex(a []byte) string {
	return hex.EncodeToString(HashRaw(a))
}

func HashAnt(a []byte) int {
	b := HashInt(a)
	if b < 0 {
		b = -b
	}
	return b
}

func HashInt(a []byte) int {
	return int(HashUnt(a))
}

func HashUnt(a []byte) uint64 {
	return binary.BigEndian.Uint64(HashRaw(a))
}

func HashI64(a []byte) int64 {
	return int64(HashUnt(a))
}

func HashA64(a []byte) int64 {
	b := HashI64(a)
	if b < 0 {
		b = -b
	}
	return b
}
