package lib

func PadL(s string, l int) string {
	for {
		if len(s) < l {
			s = " " + s
		} else {
			break
		}
	}
	return s
}

func PadR(s string, l int) string {
	for {
		if len(s) < l {
			s = s + " "
		} else {
			break
		}
	}
	return s
}
