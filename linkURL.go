package lib

import (
	"net/url"
	"strings"
)

const Quint = 1000000000000000000

func LinkUrl(link string) int {
	u, err := url.Parse(link)
	if err != nil || !strings.HasPrefix(u.Scheme, "http") || u.Host == "" {
		return 0
	}
	ID := HashAnt([]byte(u.String()))
	for {
		if ID < Quint {
			ID = ID + Quint
		} else {
			break
		}
	}
	return ID
}
